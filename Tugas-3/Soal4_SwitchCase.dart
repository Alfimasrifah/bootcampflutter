void main(List<String> args) {
  var tanggal = 31;
  var bulan = 12;
  var tahun = 2045;

  if (tanggal >= 0 && tanggal <= 31) {
    switch (bulan) {
      case 1:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Januari ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 2:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Februari ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 3:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Maret ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 4:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} April ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 5:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Mei ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 6:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Juni ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 7:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Juli ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 8:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Agustus ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 9:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} September ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 10:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Oktober ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 11:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} November ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      case 12:
        {
          if (tahun >= 1900 && tahun <= 2200) {
            print('=> ${tanggal} Desember ${tahun}');
          } else {
            print('=> tahun tidak valid, masukkan nilai antara 1900 - 2200');
          }
          break;
        }
      default:
        {
          print("=> Bulan tidak valid, masukkan nilai antara 1 - 12");
        }
    }
  } else {
    print("=> Tanggal tidak valid, masukkan nilai antara 1 - 31");
  }
}
