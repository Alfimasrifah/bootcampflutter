//Petunjuk : untuk membuat sebuah kondisi ternary dimna anda akan
//di minta untuk menginstall aplikasi dengan jawaban Y/T ,
//jadi tugas teman teman sekrang adalah memberi jawaban y/t saat ada input mau diinstall aplikasi,
//apabila ( y )maka akan menampilkan "anda akan menginstall aplikasi dart", jika (T)
// maka akan keluar pesan "aborted" (Gunakan I/O).

import 'dart:io';

void main(List<String> args) {
  print("Apakah anda ingin menginstall aplikasi ? (Y/T) ");
  String InputJawaban = stdin.readLineSync();

  if (InputJawaban == "Y") {
    print("Anda akan menginstall aplikasi dart");
  } else
    print("Aborted!");
}
