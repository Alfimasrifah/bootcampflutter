//Petunjuk : Kita akan memasuki dunia game werewolf.
//Pada saat akan bermain kamu diminta memasukkan nama dan peran.
//Untuk memulai game pemain harus memasukkan variable nama dan peran.
//Jika pemain tidak memasukkan nama maka game akan mengeluarkan warning “Nama harus diisi!“.
//Jika pemain memasukkan nama tapi tidak memasukkan peran maka game akan mengeluarkan warning
//“Pilih Peranmu untuk memulai game“. Terdapat tiga peran yaitu penyihir, guard, dan werewolf.
//Tugas kamu adalah membuat program untuk mengecek input dari pemain dan hasil response dari
//game sesuai input yang dikirimkan.

import 'dart:io';

void main(List<String> args) {
  print("====Kita Akan Memasuki Dunia Game WereWolf=====");

  String Nama;
  String Peran;

  print("Masukan Namamu :");
  Nama = stdin.readLineSync();

  print("Masukan Peranmu :");
  Peran = stdin.readLineSync();

  if (Nama == null && Peran == null) {
    print("Harus isi nama dulu!");
    Nama = stdin.readLineSync();
    print("Harus isi peran dulu!");
    Peran = stdin.readLineSync();
  }
  if (Peran == "penyihir") {
    print(" Selamat datang di Dunia Werewolf, $Nama");
    print(
        "Halo Penyihir $Nama, kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (Peran == "guard") {
    print(" Selamat datang di Dunia Werewolf, $Nama");
    print(
        "Halo Guard $Nama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else if (Peran == "werewolf") {
    print(" Selamat datang di Dunia Werewolf, $Nama");
    print("Halo Werewolf $Nama, Kamu akan memakan mangsa setiap malam!");
  } else
    (Nama == null && Peran == null);
}
