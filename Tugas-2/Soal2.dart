import 'dart:io';

void main(List<String> args) {
  var firstWord = "I";
  var secondWord = "am";
  var thirdWord = "going"; // lakukan sendiri
  var fourthWord = "to"; // lakukan sendiri
  var fifthWord = "be"; // lakukan sendiri
  var sixthWord = "flutter"; // lakukan sendiri
  var seventhWord = "developer"; // lakukan sendiri

  print('First Word: ' + firstWord);
  print('Second Word: ' + secondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);
}
