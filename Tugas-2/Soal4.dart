import 'dart:io';

void main(List<String> args) {
  print("PROGRAM OPERATOR");

  stdout.write("Nilai a: ");
  var a = double.parse(stdin.readLineSync());
  stdout.write("Nilai b: ");
  var b = double.parse(stdin.readLineSync());

  var hasil;

  // operator penjumlahan
  hasil = a + b;
  print("Penjumlahan = $hasil");

  // operator pengurangan
  hasil = a - b;
  print("Pengurangan = $hasil");

  // operator perkalian
  hasil = a * b;
  print("Perkalian = $hasil");

  // operator pembagian
  hasil = a / b;
  print("Pembagian = $hasil");
}
