void main() {
  print("Looping 1");
  for (var deret = 0; deret < 22; deret += 2) {
    print('$deret - Love Coding');
  }

  print("Looping 2");

  for (var deret = 20; deret > 0; deret -= 2) {
    print('$deret - I will become a mobile developer');
  }
}
