String introduce(String myName, int myAge, String myAddress, String myHobby) {
  var kenalan =
      "Nama saya $myName, umur saya ${myAge} tahun, alamat saya di $myAddress, dan saya punya hobby yaitu $myHobby!";
  return kenalan;
}

void main(List<String> args) {
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogyakarta";
  var hobby = "Gaming";

  var perkenalan = introduce(name, age, address, hobby);
  print(perkenalan);
}
