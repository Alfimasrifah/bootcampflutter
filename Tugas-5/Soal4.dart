int factorial(int bil) {
  var result = bil;

  if (bil <= 0) {
    result = 1;
  } else {
    for (int i = bil - 1; i > 1; i--) {
      result = result * i;
    }
  }

  return result;
}

void main(List<String> args) {
  int number = 5;
  var value = factorial(number);
  print("Hasil faktorial ${number} adalah ${value}");
}
