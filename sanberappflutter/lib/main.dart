import 'package:flutter/material.dart';
import 'package:sanberappflutter/home_screen.dart';
//import 'package:sanberappflutter/SplashScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}
